#!/bin/bash
COLORS=$HOME/.cache/wal/colors

BG=$(sed -n 1p "$COLORS")
color1=$(sed -n 2p "$COLORS")
color2=$(sed -n 3p "$COLORS")
color3=$(sed -n 4p "$COLORS")
color4=$(sed -n 5p "$COLORS")
color5=$(sed -n 6p "$COLORS")
color6=$(sed -n 7p "$COLORS")
color7=$(sed -n 8p "$COLORS")
color8=$(sed -n 9p "$COLORS")
color9=$(sed -n 10p "$COLORS")
color10=$(sed -n 11p "$COLORS")
color11=$(sed -n 12p "$COLORS")
color12=$(sed -n 13p "$COLORS")
color13=$(sed -n 14p "$COLORS")
color14=$(sed -n 15p "$COLORS")
FG=$(sed -n 16p "$COLORS")
